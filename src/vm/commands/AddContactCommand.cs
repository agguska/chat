﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.src.vm.commands
{
    public class AddContactCommand : ICommand
    {
        private readonly Action actionToExecute;
        public AddContactCommand(Action action)
        {
            actionToExecute = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            actionToExecute();
        }
    }


}
