﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp1.src.vm.commands;

namespace WpfApp1.src.vm
{
    class VM_Chat : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Person> contactList = new ObservableCollection<Person>();
        public ICommand SendMessageCmd { get; set; }
        public ICommand AddContactCmd { get; set; }
        public string newMessage { get; set; }
        public string newContact { get; set; }
        public ObservableCollection<Person> requestList = new ObservableCollection<Person>();

        public VM_Chat()
        {
            List<Person> contacts = Helper.GetInstance.getContacts();
            foreach(var item in contacts)
            {
                this.contactList.Add(item);
            }
            SendMessageCmd = new SendMessageCommand(param => sendMessage(param));
            AddContactCmd = new AddContactCommand(addContact);
        }

        private void sendMessage(object receiver)
        {
            ((Person)receiver).sendMessage(newMessage);
            newMessage = null;
            NotifyPropertyChanged("newMessage");
        }

        private void addContact()
        {
            string contactNickname = this.newContact;
            Helper.GetInstance.getRequests();
            //TODO send public key request- message content = "GIVE KEY"
        }

        public ObservableCollection<Person> RequestList
        {
            get { return requestList; }
            set { NotifyPropertyChanged(ref requestList, value); }
        }

        public ObservableCollection<Person> ContactList {
            get { return contactList; }
            set { NotifyPropertyChanged(ref contactList, value); }
        }
    
        public void NotifyPropertyChanged(string nomPropriete)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(nomPropriete));
        }

        public bool NotifyPropertyChanged<T>(ref T variable, T valeur, [CallerMemberName] string nomPropriete = null)
        {
            if (object.Equals(variable, valeur)) return false;

            variable = valeur;
            NotifyPropertyChanged(nomPropriete);
            return true;
        }

    }
}
