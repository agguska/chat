﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.src.model
{
    public class CDMMessage
    {
        public string id { get; set; }
        public string msg { get; set; }
        public string receiver { get; set; }
        public string author { get; set; }
        public string dateCreated { get; set; }
        public string alreadyReturned { get; set; }

        public CDMMessage(string message, string receiver)
        {
            this.msg = message;
            this.receiver = receiver;
        }
    }
}
