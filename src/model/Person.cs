﻿using System;
﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.src;
using WpfApp1.src.services;

namespace WpfApp1
{
    [Table("Person")]
    public class Person: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        [Column("nickname")]
        public string nickName { get; set; }
        [Column("lastSeen")]
        public DateTime lastSeen { get; set; }
        public CustomCryptoProvider cp { get; set; }
        List<Person> contacts { get; set; }

        public int Id
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }
        public string NickName
        {
            set
            {
                nickName = value;
            }
            get
            {
                return nickName;
            }
        }
        public DateTime LastSeen
        {
            set
            {
                lastSeen = value;
            }
        }

        public Person() { }

        public Person(int id, string nickName, DateTime lastSeen)
        {
            this.id = id;
            this.nickName = nickName;
            this.lastSeen = lastSeen;
            this.cp = new CustomCryptoProvider();
        }

        public override string ToString()
        {
            return id + " " + nickName + " - last seen " + lastSeen;
        }

        public void sendMessage(string m)
        {
            Helper.GetInstance.sendMessage(m, this);
            NotifyPropertyChanged("Messages");

        }

        public List<Message> Messages
        {
            get
            {
                return Helper.GetInstance.getMessages(this);
            }
        }

        public void NotifyPropertyChanged(string nomPropriete)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(nomPropriete));
        }

    }
}
