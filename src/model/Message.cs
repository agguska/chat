﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{

    public class Message
    {
        public string id { get; set; }
        public string message { get; set; }
        public Person from { get; set; }
        public Person to { get; set; }
        public DateTime date { get; set; }

        public Message() { }

        public Message(Message message)
        {
            this.id = message.id;
            this.message = message.message;
            this.from = message.from;
            this.to = message.to;
            this.date = message.date;
        }

        public Message(string id, string message, Person from, Person to, DateTime date)
        {
            this.id = id;
            this.message = message;
            this.from = from;
            this.to = to;
            this.date = date;
        }

        public override string ToString()
        {
            return "Message: " + message + " - send by " + from + " to " + to + " on " + date;
        }
    }
}
