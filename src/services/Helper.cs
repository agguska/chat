﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.src.model;
using WpfApp1.src.services;

namespace WpfApp1.src { 


    public class Helper
    {
        private static Helper instance = null;
        public static Helper GetInstance { 
            get
            {
                if (instance == null)
                {
                    instance = new Helper();
                }
                return instance;
            }
        }
        private static List<Person> persons = new List<Person>();
        private static List<Message> messages = new List<Message>();
        private static List<Message> sentMessages = new List<Message>();
        private static Person loggedUser;
        private static string loggedUserToken { get; set; }
        private ApiService api = new ApiService();

        private Helper()
        {
            init();
        }

        private void init()
        {
            loggedUser = new Person(0, "Me", DateTime.Now);

            Person p1 = new Person(1, "Admin", DateTime.Now);
            Person p2 = new Person(2, "User11", DateTime.Now);
            Person p3 = new Person(3, "Selma", DateTime.Now);
            persons.Add(p1);
            persons.Add(p2);
            persons.Add(p3);
            persons.Add(loggedUser);

            /*sendMessage("Message1", loggedUser);
            sendMessage("Message2", p1);
            sendMessage("Message3", p2);
            sendMessage("Message4", p3);
            sendMessage("Message5", p1);
            sendMessage("Message6", p2);
            sendMessage("Message7", p3);
            sendMessage("Message8", p1);
            sendMessage("Message9", p2);
            sendMessage("Message10", p3);
            sendMessage("Message11", loggedUser);
            sendMessage("Message12", loggedUser);*/

            login();
            fetchMessages();


        }

        public void login()
        {
            string loginToken = api.login("Agnieszka", "pass");
            Console.WriteLine("LOGGED: " + loginToken);
            loggedUserToken = loginToken;
        }

        public void fetchMessages()
        {
            CDMMessage[] messageArray = api.fetchMessages(loggedUserToken);
            if (messageArray != null)
            {
                messages.RemoveRange(0, messages.Count);
                foreach (CDMMessage msg in messageArray)
                {
                    DateTime date = DateTime.ParseExact(msg.dateCreated, "yyyy-MM-ddTHH:mm:ssZ",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    Message message = new Message(msg.id, msg.msg, new Person(10, msg.author, date), null, date);
                    messages.Add(message);
                }
            }
        }

        public void sendMessage(String newMesssageContent, Person receiver)
        {
            string encryptedMessage = receiver.cp.encrypt(newMesssageContent);
            CDMMessage newMessage = new CDMMessage(encryptedMessage, receiver.nickName);
            CDMMessage returnedMessage = api.sendMessage(newMessage, loggedUserToken);

            if (returnedMessage != null)
            {
                DateTime date = DateTime.ParseExact(returnedMessage.dateCreated, "yyyy-MM-ddTHH:mm:ssZ",
                                           System.Globalization.CultureInfo.InvariantCulture);
                Message message = new Message(returnedMessage.id, returnedMessage.msg, new Person(10, returnedMessage.author, date), receiver, date);
                sentMessages.Add(message);
            }

        }

        public List<Message> getMessages(Person person)
        {
            fetchMessages();
            List<Message> filteredMessages =  messages
                .Where(mess => (mess.from != null && mess.from.nickName.Equals(person.nickName)))
               // .Select(message => {
                   // var decryptedMessage = new Message(message);
                    //decryptedMessage.message = person.cp.decrypt(message.message);
                 //   return decryptedMessage;
               // })
                .ToList();

            List<Message> filteredSentMessages = sentMessages
               .Where(mess => (mess.to != null && mess.to.nickName.Equals(person.nickName)))
               // .Select(message => {
               // var decryptedMessage = new Message(message);
               //decryptedMessage.message = person.cp.decrypt(message.message);
               //   return decryptedMessage;
               // })
               .ToList();

            filteredMessages.AddRange(filteredSentMessages);
            return filteredMessages;
        }

        public List<Message> getRequests() // requests for public keys - has to be accepted/declined
        {
            return messages;
        }

        public List<Person> getContacts()
        {
            return persons;
        }

        public List<Message> getMessages()
        {
            return messages;
        }

        public void showAll(ref List<Person> persons)
        {
            foreach (Person obj in persons)
            {
                Console.WriteLine("----> " + obj);
            }

        }

        public void addPerson(ref List<Person> list)
        {
            Person person = new Person();
            Console.WriteLine("Nick name? ");
            person.nickName = Console.ReadLine();
            person.lastSeen = DateTime.Now;
            person.id = list.Count + 1;
            list.Add(person);
        }

        public void filter(ref List<Person> list, ref List<Message> mList)
        {
            Console.WriteLine("Do you want to filter by the nickname first letter? If no, press 0; if yes, insert a letter: ");
            string c = Console.ReadLine();

            Console.WriteLine("Do you want to filter by the number of messages? If no, press n; if yes, insert the minimum number: ");
            string c2 = Console.ReadLine();
            Int32 numberOfMessages = 0;
            if (c2.ToLower()[0] != 'n')
            {
                try { numberOfMessages = Int32.Parse(c2); }
                catch
                {
                    Console.WriteLine("Wrong number... Stop it! ");
                }
            }
        }

        /*public void countMessages(ref List<Person> list, ref List<Message> mList)
        {
            var selectedPersons =
                 from pers in list
                 join message in mList on pers equals message.to into sentMessages
                 join message in mList on pers equals message.from into receivedMessages
                 select new { nickname = pers.nickName, total = sentMessages.Count() + receivedMessages.Count(), sent = sentMessages.Count(), received = receivedMessages.Count() };

            foreach (var item in selectedPersons)
            {
                Console.WriteLine(item.nickname + " has in total " + item.total + " messages. Sent - " + item.sent + ", got - " + item.received);
            }
        }*/

        public void save(ref List<Person> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Person person in list)
            {
                sb.Append('\n');
                foreach (FieldInfo field in typeof(Person).GetFields())
                {
                    foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                    {
                        sb.Append(field.GetValue(person));
                        sb.Append(";");
                    }
                }
            }
            using (StreamWriter writetext = new StreamWriter("sentMessages.txt")) { writetext.Write(sb.ToString()); }
        }

    }
}
