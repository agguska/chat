﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.src.model;

namespace WpfApp1.src.services
{
    class ApiService
    {
        private const string URL = "http://baobab.tokidev.fr/";
        private RestClient server = new RestClient(URL);

        public CDMMessage sendMessage(CDMMessage msg,  string token)
        {

            var request = new RestRequest("api/sendMsg", Method.POST);

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddJsonBody(new
            {
                message = msg.msg,
                receiver = msg.receiver
            });

            IRestResponse response = this.server.Execute(request);
            var json = response.Content;
            if (json != null)
            {
                CDMMessage message = JsonConvert.DeserializeObject<CDMMessage>(json);
                return message;
            }
            return null;
        }

        public string login(string username, string password)
        {
            var request = new RestRequest("api/login", Method.POST);

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(new
            {
                username = username,
                password = password
            });

            IRestResponse response = this.server.Execute(request);
            var json = response.Content;
            if (json != null)
            {
                CDMLoginResponse data = JsonConvert.DeserializeObject<CDMLoginResponse>(json);
                return data.access_token;
            }
            return null;
        }

        public CDMMessage[] fetchMessages(string token)
        {
            var request = new RestRequest("api/fetchMessages", Method.GET);

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);

            IRestResponse response = this.server.Execute(request);
            var json = response.Content;
            List<Message> messageList = new List<Message>();
            if (json != null)
            {
                return JsonConvert.DeserializeObject<CDMMessage[]>(json);
            }
            return null;
        }
    }
}
